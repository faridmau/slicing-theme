<?php
  /**
   * Script untuk Send Email
   */
  $strError = "";
  //email pengirim
  $email_submit = "venario@rectmedia.com";

  //BILA PAKAI RECAPTCHA
 /* require_once('./js/recaptchalib.php');
  //$options = get_option( 'recaptcha_options' );
  $publickey = '6LdRf8kSAAAAAOWawp99hLoQLPnOBkktantW9fJ3';
  $privatekey = '6LdRf8kSAAAAAGsfoMgupEcsnZYfezDDZrqWjUW_';
  
 
  //the response from reCAPTCHA
  $resp = null;
  # the error code from reCAPTCHA, if any
  $error = null;
  //SAMPAI DI SINI RECAPTCHA*/
  if (isset($_POST['action']) == "send") {
  $name = $_POST["name"];
  $email = $_POST["email"];
  $telepon = $_POST["phone"];
  $subject = $_POST["subject"];
  $message = $_POST["pesan"];  
  $strMessage = '
          Hi,
          <br />
          Seseorang mengirim email melalui Form Contact dari website www.mahesanet.com, berikut detailnya :
          <br />
          Data Diri :
          <table width="400px" style="margin-left:40px">
          <tr><td width="30%">Nama :</td><td>'.$name.'</td></tr>
          <tr><td width="30%">Email :</td><td>'.$email.'</td></tr>
          <tr><td width="30%">Telepon :</td><td>'.$telepon.'</td></tr>
          </table>
          <br />
          Pesan yang disampaikan :
          <table width="400px" style="margin-left:40px">
          <tr>
            <td width="30%">Judul :</td><td>'.$subject.'</td>
          </tr>
          <tr>
            <td width="30%">Pesan :</td><td>'.$message.'</td>
          </tr>
          </table>
      ';

    $strTo = $email_submit;
    $strSubject = '[PARADISE] Anda mendapatkan pesan dari '.$name.' melalui portal pesan www.mahesanet.com';
    $strMessage = nl2br($strMessage);

    $strSid = md5(uniqid(time()));

    $strHeader = "";
    $strHeader .= "From: ".$name."<".$email.">\nReply-To: ".$email."\n";

    $strHeader .= "MIME-Version: 1.0\n";
    $strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\n\n";
    $strHeader .= "This is a multi-part message in MIME format.\n";

    $strHeader .= "--".$strSid."\n";
    $strHeader .= "Content-type: text/html; charset=utf-8\n";
    $strHeader .= "Content-Transfer-Encoding: 7bit\n\n";
    $strHeader .= $strMessage."\n\n";

          # was there a reCAPTCHA response?
          # JIKA PAKAI RECAPTCHA
      /*  if ($_POST["recaptcha_response_field"]) {
                  $resp = recaptcha_check_answer ($privatekey,
                                                  $_SERVER["REMOTE_ADDR"],
                                                  $_POST["recaptcha_challenge_field"],
                                                  $_POST["recaptcha_response_field"]);

                  if ($resp->is_valid) {*/
                      $flgSend = @mail($strTo,$strSubject,null,$strHeader);  // @ = No Show Error //

                      if($flgSend)
                      {
                              $strError = '<h3 id="successmessage">Terima kasih, pesan berhasil terkirim</h3>';
                      }
                      else
                      {
                              $strError = '<h3 id="errormessage">Maaf, pesan gagal terkirim</h3>';
                      }
          // JIKA PAKAI RECAPTCHA              
          /*                          
                  } else {
                          # set the error code so that we can display it
                          $error = $resp->error;
                          $strError = "<br><center><h2>".$error."</h2></center><br>";
                  }
          }*/

  }
  
  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Mahesa Computer | One Stop Computer & Networking Needs</title>
        <meta name="description" content="We're your One Stop Computer & Networking needs"/>
        <meta name="keywords" content="Computer, Server, Cabling"/>
        <meta name="robots" content="index,follow" />
        <meta name="GOOGLEBOT" content="archive" />
        <meta name="author" content="Cloud Paradise"/>
        <link rel="image_src" href="images/mahesa.jpg" />
        <link rel="icon" href="images/favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style/style.css"/>
        <link rel="stylesheet" type="text/css" href="style/960.css"/>
        <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
        <link rel="stylesheet" type="text/css" href="style/validationEngine.jquery.css" />
	      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHAd5EtGvzkpyMJ3C_qNEb81ujYCB7rEA&sensor=false"></script>
	      <script type="text/javascript" src="js/map.js"></script>
	      <script type="text/javascript" src="js/jquery.validationEngine-en.js"></script>
	      <script type="text/javascript" src="js/jquery.validationEngine.js"></script>
    </head>
    
    <body>
    	<div class="black-line"></div>
    	<div class="header-wrap">
        <div class="offset-wrapp">
          
        
      		<div id="header" class="container_12">
      			<div class="img-top"><img src="images/mac.png" alt="mahesa komputer"/></div>
      			<div class="grid_7 suffix_5">
  	    			<div class="logo"><a href="index.php"><img src="images/logo.png" alt="mahesa komputer"/></a></div>
  	    			<div class="offer">
  	    				<h2>We're your One Stop Computer And Networking needs.</h2>
  	    				<div class="motto">
  	    					<p class="bottom-line"><strong>Kualitas terpercaya untuk solusi komputerisasi Anda</strong></p>
  		    				<p><span class="red">Mahesa Computer</span> menyediakan segala <strong>kebutuhan komputer</strong> dan 
  									perlengkapan <strong>networking</strong> dengan kualitas yang unggul. 
  									Layanan profesional kami akan membuat Anda nyaman dalam 
  									mengembangkan sistem komputerisasi usaha Anda. Kami juga 
  									memberikan garansi <span class="italic">after sales</span> untuk pelanggan - pelanggan setia kami.</p>

  	    				</div>
  	    				
  	    				<div class="button-wrapp">
  								<a href="http://mahesanet.com/dec2011intelservermix.pdf" class="button" target=_black>SPECIAL OFFERS</a>
  							</div>
  	    			</div>
  	    		</div>
  	    		<div class="clear"></div>
  	    		
      		</div>
    		</div>
    	</div>
    	<div class="special-wrapp">
    		<div id="special" class="container_12">
    			<div class="grid_12">
    				<h4>SPECIALIZED ON</h4>
    				<img src="images/img2.jpg" alt="mahesa komputer"/>
    			</div>
    			<div class="clear"></div>
    		</div>
    	</div>
    	<div id="why-wrapper">
    		<div id="why" class="container_12">
    			<div class="img2"><img src="images/server.png" alt="mahesa komputer"/></div>
    			<div class="grid_6 prefix_6">
    				<h3>Why Mahesa Computer?</h3>
    				<ul>
    					<li>
    						<div class="left circle"><p>1</p></div>
    						<div class="why-text">
    							<p class="red font-bold">Professional Service</p>
    							<p>Staff Tenaga ahli berpengalaman dengan jam terbang yang tinggi, kami pastikan service kepada Anda selalu dilakukan secara professional.</p>
    						</div>
    					</li>
    					<li>
    						<div class="left circle"><p>2</p></div>
    						<div class="why-text">
    							<p class="red font-bold">Fast Response</p>
    							<p>Tersedianya tim service dan teknis memastikan Anda mendapatkan respons yang cepat untuk solusi kebutuhan IT Anda.</p>
    						</div>
    						<div class="clear"> </div>
    					</li>
    					<li>
    						<div class="left circle"><p>3</p></div>
    						<div class="why-text">
    							<p class="red font-bold">Guaranteed Product</p>
    							<p>Semua product yang kami distribusikan bergaransi resmi global dan bukan barang loose-pack ataupun pre-owned.</p>
    						</div>
    						<div class="clear"> </div>
    					</li>
    					<li>
    						<div class="left circle"><p>4</p></div>
    						<div class="why-text">
    							<p class="red font-bold">On-Site Service Support</p>
    							<p>Kami menyediakan on site service support langsung di tempat Anda, untuk memastikan downtime yg Anda alami selalu minimal.</p>
    						</div>
    						<div class="clear"> </div>
    					</li>
    					<li>
    						<div class="left circle"><p>5</p></div>
    						<div class="why-text">
    							<p class="red font-bold">All Around Solution</p>
    							<p>Solusi hardware, solusi software, pekerjaan lapangan, network, serta cabling. Tenaga teknis terpadu kami siap memenuhi semua kebutuhan di tempat/perusahaan yang Anda kelola.</p>
    						</div>
    						<div class="clear"> </div>
    					</li>
    				</ul>
    			</div>
    			<div class="clear"></div>
    		</div>
    		
    	</div>
    	<div id="contact-wrapp">
    		<div id="contact" class="container_12">
    			<div class="grid_5 form-con">
    				<p class="p72">CONTACT</p>
	    			<form id="contact-form"  name="contact_form" action="index.php" method="post">
              <input type="hidden" name="action" value="send" />
	    				<ul>
                <li><?php echo $strError; ?></li>
	    					<li><input type="text" name="name" class="validate[required]" placeholder="name"/></li>
	    					<li><input type="text" name="email" class="validate[required,custom[email]]" placeholder="email"/></li>
	    					<li><input type="text" name="phone" class="validate[required,custom[integer],maxSize[20]]" placeholder="phone"/></li>
	    					<li><input type="text" name="subject" class="validate[required]" placeholder="subject"/></li>
	    					<li><textarea name="pesan" placeholder="your message" class="validate[required]"></textarea></li>
	    					<li><input type="submit" class="button" value="SEND MESSAGE"></li>
	    				</ul>
	    			</form>
    			</div>
          <div class="contact-w">
            
            <div class="coo left">
              <div id="map"></div>
              <div class="inform">
                <div class="call">
                  <div class="up">
                    <h4 class="red">CALL US</h4>
                    <p class="font48">024 - 8451600</p>
                    <p>Senin - Jumat</p>
                    <p>08.00 - 17.00 WIB</p>
                    <p>Sabtu</p>
                    <p>08:00 - 14:00 WIB</p>
                  </div>
                  <div class="down">
                    <h4>VISIT US</h4>
                    <p>Jl. Singosari Raya No 53 Semarang</p>
                    <p><span class="red font12">email</span> mahesa@mahesanet.com</p>
                    <p><span class="red font12">sms</span> 08179555511</p>
                    <p><span class="red font12">fax</span> 024 - 8416169</p>
                  </div>
                  <div class="chat">
                    <h4>CHAT WITH US</h4>
                    <ul>
                      <li>
                        <p class="y-name font18 red left">Martin</p><span><a href="ymsgr:sendIM?maesa53"> <img src="http://opi.yahoo.com/online?u=maesa53&m=g&t=2&l=us" alt="Mahesa"/></a></span>
                        <div class="clear"></div>
                      </li>
                      <li>
                        <p class="y-name font18 red left">Krisma</p><span><a href="ymsgr:sendIM?mahesa53"> <img src="http://opi.yahoo.com/online?u=mahesa53&m=g&t=2&l=us" alt="Mahesa"/></a></span>
                        <div class="clear"></div>
                      </li>
                      <li>
                        <p class="y-name font18 red left">Iyan</p><span><a href="ymsgr:sendIM?mahesa0053"> <img src="http://opi.yahoo.com/online?u=mahesa0053&m=g&t=2&l=us" alt="Mahesa"/></a></span>
                        <div class="clear"></div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <img src="images/contact-bg.png" style="position:absolute">
            </div>
            
          </div>
          <div class="clear"></div>
    		</div>
    	</div>
    	<div id="foo-wrapp">
    		<div id="footer" class="container_12">
    			<div class="grid_9">
    				<p>Copyright &copy; 2013.</p>
    				<p>Hak cipta terhadap seluruh media yang terdapat di dalam website ini adalah milik  <span class="red">MAHESA COMPUTER</span></p>
    			</div>
    			<div class="grid_3"><a href="http://rectmedia.com" target=_blank><img src="images/paradise.png" alt="Cloud Paradise | Website Murah"/></a></div>
    			<div class="clear"></div>
    		</div>
    	</div>
    	<script type="text/javascript">
    	$(document).ready(function(){
    		initializeMap();
        $("#contact-form").validationEngine();
        
    	});
    	</script>
    </body>
</html>
