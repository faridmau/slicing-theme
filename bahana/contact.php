<?php
  /**
   * Script untuk Send Email
   */
  $strError = "";
  //email pengirim
  $email_submit = "venario@rectmedia.com";

  //BILA PAKAI RECAPTCHA
 /* require_once('./js/recaptchalib.php');
  //$options = get_option( 'recaptcha_options' );
  $publickey = '6LdRf8kSAAAAAOWawp99hLoQLPnOBkktantW9fJ3';
  $privatekey = '6LdRf8kSAAAAAGsfoMgupEcsnZYfezDDZrqWjUW_';
  
 
  //the response from reCAPTCHA
  $resp = null;
  # the error code from reCAPTCHA, if any
  $error = null;
  //SAMPAI DI SINI RECAPTCHA*/
  if (isset($_POST['action']) == "send") {
  $name = $_POST["name"];
  $email = $_POST["email"];
  $telepon = $_POST["phone"];
  $subject = $_POST["subject"];
  $message = $_POST["pesan"];  
  $strMessage = '
          Hi,
          <br />
          Seseorang mengirim email melalui Form Contact dari website www.kolamku.com, berikut detailnya :
          <br />
          Data Diri :
          <table width="400px" style="margin-left:40px">
          <tr><td width="30%">Nama :</td><td>'.$name.'</td></tr>
          <tr><td width="30%">Email :</td><td>'.$email.'</td></tr>
          <tr><td width="30%">Telepon :</td><td>'.$telepon.'</td></tr>
          </table>
          <br />
          Pesan yang disampaikan :
          <table width="400px" style="margin-left:40px">
          <tr>
            <td width="30%">Judul :</td><td>'.$subject.'</td>
          </tr>
          <tr>
            <td width="30%">Pesan :</td><td>'.$message.'</td>
          </tr>
          </table>
      ';

    $strTo = $email_submit;
    $strSubject = '[PARADISE] Anda mendapatkan pesan dari '.$name.' melalui portal pesan www.kolamku.com';
    $strMessage = nl2br($strMessage);

    $strSid = md5(uniqid(time()));

    $strHeader = "";
    $strHeader .= "From: ".$name."<".$email.">\nReply-To: ".$email."\n";

    $strHeader .= "MIME-Version: 1.0\n";
    $strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\n\n";
    $strHeader .= "This is a multi-part message in MIME format.\n";

    $strHeader .= "--".$strSid."\n";
    $strHeader .= "Content-type: text/html; charset=utf-8\n";
    $strHeader .= "Content-Transfer-Encoding: 7bit\n\n";
    $strHeader .= $strMessage."\n\n";

          # was there a reCAPTCHA response?
          # JIKA PAKAI RECAPTCHA
      /*  if ($_POST["recaptcha_response_field"]) {
                  $resp = recaptcha_check_answer ($privatekey,
                                                  $_SERVER["REMOTE_ADDR"],
                                                  $_POST["recaptcha_challenge_field"],
                                                  $_POST["recaptcha_response_field"]);

                  if ($resp->is_valid) {*/
                      $flgSend = @mail($strTo,$strSubject,null,$strHeader);  // @ = No Show Error //

                      if($flgSend)
                      {
                              $strError = '<h3 id="successmessage">Terima kasih, pesan berhasil terkirim</h3>';
                      }
                      else
                      {
                              $strError = '<h3 id="errormessage">Maaf, pesan gagal terkirim</h3>';
                      }
          // JIKA PAKAI RECAPTCHA              
          /*                          
                  } else {
                          # set the error code so that we can display it
                          $error = $resp->error;
                          $strError = "<br><center><h2>".$error."</h2></center><br>";
                  }
          }*/

  }
  
  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Kontak Kolamku | Bahana Fantastic Pool | Jasa Pembuatan Kolam Renang </title>
      <meta name="description" content="Sebagai salah satu penyedia jasa pembuatan kolam renang, Kolamku.com atau lebih dikenal Bahana fantastic pool juga merupakan perusahaan yang bergerak di bidang water treatment & cleaning chemicals, perencanaan pembuatan dan perawatan kolam renang."/>
      <meta name="keywords" content="kolamku, jasa pembuatan kolam renang, kolam renang"/>
      <meta name="robots" content="index,follow" />
      <meta name="GOOGLEBOT" content="archive" />
      <meta name="author" content="Cloud Paradise"/>
      <link rel="image_src" href="images/crawl.jpg" />
      <link rel="icon" href="images/favicon.png"/>
      <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" type="text/css" href="style/960.css" />
  	  <link rel="stylesheet" type="text/css" href="style/style.css" />
      <link rel="stylesheet" type="text/css" href="js/validationEngine.jquery.css" />
      <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHAd5EtGvzkpyMJ3C_qNEb81ujYCB7rEA&sensor=false"></script>
      <script type="text/javascript" src="js/map.js"></script>
      <script type="text/javascript" src="js/jquery.validationEngine.js"></script> 
      <script type="text/javascript" src="js/jquery.validationEngine-en.js"></script>  
      <script src="js/css_browser_selector.js" type="text/javascript"></script>    
    </head>
    
    <body>
      <?php include"header.php"; ?>
      <div class="wrapper content-wrapper contact-wrapper">
        <div id="content contact" class="container_12">
          <div class="grid_12">
            <div class="blue-line"></div>
          </div>
          <div class="clear"></div>
          <div class="grid_12">
            <h2>Contact Us</h2>
          </div>
          <div class="clear"></div>
          <div class="grid_8 left-contact">
            
            <div id="map"></div>
            <p><strong>BAHANA FANTASTIC POOL</strong> siap membantu Anda, silahkan sampaikan pesan, kritik saran maupun masalah yang ingin Anda tanyakan kepada kami :</p>
            <p><?php echo $strError; ?></p>
            <form id="contact_form" name="contact_form" action="contact.php" method="post">
              <input type="hidden" name="action" value="send" />
              <table>
                <tr>
                  <td><label>Nama</label></td>
                  <td>:</td>
                  <td><input type="text" name="name" class="validate[required]" id="name"  value=""></td>
                </tr>
                <tr>
                  <td><label>Email</label></td>
                  <td>:</td>
                  <td><input type="text" name="email" class="validate[required,custom[email]]" id="email" value=""></td>
                </tr>
                <tr>
                  <td><label>Telepon</label></td>
                  <td>:</td>
                  <td><input type="text" name="phone"  class="validate[required,custom[integer],maxSize[20]]" id="phone" value=""></td>
                </tr>
                <tr>
                  <td><label>Subject</label></td>
                  <td>:</td>
                  <td><input type="text" name="subject" class="validate[required]" id="subject" value=""></td>
                </tr>
                <tr>  
                  <td style="vertical-align: top;padding-top:10px;"><label>Pesan</label></td>
                  <td style="vertical-align: top;padding-top:10px;">:</td>
                  <td><textarea type="textarea" name="pesan" class="validate[required]" id="pesan" value=""></textarea></td>
                </tr> 
                
              </table>
              <div class="form-button">
                <input type="submit" class="button gradien-blue" value="Submit">
              </div>
              
            </form>
          </div>
          <div class="grid_4 right-contact">
            <div class="box-thumb">
              <img src="images/thumb5.jpg" alt="kolam renang" class="radius">
            </div>
            <h5 class="blue">Kantor Pusat</h5>
            <p>Jl. Wahyu Temurun 2 no 16<br>Tlogosari Semarang</p>
            <br>
            <table>
              <tr>
                <td>Phone</td>
                <td>:</td>
                <td>(024) 702 27 445, 081 664 2975</td>
              </tr>
              <tr>
                <td>Pin BB</td>
                <td>:</td>
                <td>276DF303</td>
              </tr>
              <tr>
                <td>Email</td>
                <td>:</td>
                <td>bahanawatertreathment@yahoo.co.id</td>
              </tr>
            </table>
            <br><br>
   
          </div>
          <div class="clear"></div>
        </div>
        
      </div>
      <?php include"footer.php"; ?>
      <script type="text/javascript">
        $(window).ready(function() {
          initializeMap();
		  $("form#contact_form").validationEngine();
        });
      </script>
    </body>
</html>