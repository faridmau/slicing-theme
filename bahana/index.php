<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Kolamku | Bahana Fantastic Pool | Jasa Pembuatan Kolam Renang </title>
      <meta name="description" content="Sebagai salah satu penyedia jasa pembuatan kolam renang, Kolamku.com atau lebih dikenal Bahana fantastic pool juga merupakan perusahaan yang bergerak di bidang water treatment & cleaning chemicals, perencanaan pembuatan dan perawatan kolam renang."/>
      <meta name="keywords" content="kolamku, jasa pembuatan kolam renang, kolam renang"/>
      <meta name="robots" content="index,follow" />
      <meta name="GOOGLEBOT" content="archive" />
      <meta name="author" content="Cloud Paradise"/>
      <link rel="image_src" href="images/paradise.jpg" />
      <link rel="icon" href="images/favicon.png"/>
      <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" type="text/css" href="style/960.css" />
  		<link rel="stylesheet" type="text/css" href="style/style.css" />
      <link rel="stylesheet" type="text/css" href="style/nivo-slider/nivo-slider.css" />
      <link rel="stylesheet" type="text/css" href="style/nivo-slider/default.css" />
      <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
      <!--<script type="text/javascript" src="js/nivo-slider/jquery.nivo.slider.js"></script>-->
      <script type="text/javascript" src="js/nivo-slider/jquery.nivo.slider.pack.js"></script>
      <script src="js/css_browser_selector.js" type="text/javascript"></script>
    </head>
    
    <body>
      <?php include"header.php"; ?>
      <div class="wrapper sldier-wrapper">
        <div class="slider theme-default">
          <div class="nivo-slider">
            <img src="images/slider1.jpg" alt="jasa-pembuatan-kolam-renang-kolamku-slide-one"/>
            <img src="images/slider2.jpg" alt="jasa-pembuatan-kolam-renang-kolamku-slide-two"/>
            <img src="images/slider3.jpg" alt="jasa-pembuatan-kolam-renang-kolamku-slide-three"/>
          </div>
        </div>
      </div>
      <div class="wrapper content-wrapper">
        <div id="content" class="container_12">
          <div id="feature">
            <div class="grid_3">
              <div class="feature-box">
                <h3>SWIMMING POOL</h3>
                <div class="box-thumb small-img">
                  <img src="images/thumb1.jpg" alt="swimming pool" class="radius"/>
                </div>
                <p>Kami selain merencanakan, design, gambar kerja hingga gambar 3 dimensi pembuatan kolam renang juga menyediakan spare part dan perlengkapan kolam renang dengan harga spesial. </p>
                
              </div>
              <div class="read-more">
                <a href="product.php#swimming" class="button gradien-blue">Read More</a>
              </div>
            </div>
            <div class="grid_3">
              <div class="feature-box">
                <h3>WATER TREATMENT</h3>
                <div class="box-thumb small-img">
                  <img src="images/thumb2.jpg" alt="water treatment" class="radius"/>
                </div>
                <p>Bahana water treatment adalah satu divisi dari bahana fantastic pool yang bergerak di bidang perlengkapan water treatment dan purifier untuk kebutuhan rumah tangga, industri, rumah sakit, hotel, restoran, ruko, <span class="italic">sport club</span>, pabrik air minum.</p>
                
              </div>
              <div class="read-more">
                <a href="product.php#water" class="button gradien-blue">Read More</a>
              </div>
            </div>
            <div class="grid_3">
              <div class="feature-box">
                <h3>CHEMICAL WATER</h3>
                <div class="box-thumb small-img">
                  <img src="images/thumb3.jpg" alt="chemical water" class="radius"/>
                </div>
                <p>Perusahaan distributor dan supplier bahan kimia yang berkantor di Semarang Jawa Tengah untuk industri water treatment, kolam renang, tambak, dll.</p>
                
              </div>
              <div class="read-more">
                <a href="product.php#chemical" class="button gradien-blue">Read More</a>
              </div>
            </div>
            <div class="grid_3">
              <div class="feature-box">
                <h3>POOL TOOLS</h3>
                <div class="box-thumb small-img">
                  <img src="images/thumb4.jpg" alt="pool tools" class="radius"/>
                </div>
                <p>Perusahan kami juga menyediakan dan menggunakan perlengkapan yang terbaik untuk menjaga kualitas pengerjaan kami maupun perawatan kolam renang anda.</p>
                
              </div>
              <div class="read-more">
                <a href="product.php#swimming" class="button gradien-blue">Read More</a>
              </div>
            </div>

            <div class="clear"></div>
          </div>
          <div id="welcome">
            <div class="grid_8">
              <h2>Welcome</h2>
              <p><span class="blue">Bahana fantastic pool</span> adalah perusahaan yang bergerak di bidang <span class="italic">water treatment & cleaning chemicals</span>, perencanaan pembuatan dan perawatan kolam renang. Dalam perkembanganya kami juga menjadi distributor <span class="italic">chemicals</span> untuk kolam renang seperti chlorine, PAC, soda ash dan lainya. Adapun visi dan misi kami adalah untuk mewujudkan impian anda untuk membuat kolam pribadi, <span class="italic">sport club</span> ataupun untuk umum tentunya dengan mengutamakan mutu dan kualitas yang terbaik serta harga yang terjangkau dengan garansi yang kami jamin. Pekerjaan yang transparan dan tepat waktu adalah komitmen kami.</p>
              <div class="read-more right">
                <a class="blue" href="#">Read More...</a>
              </div>
            </div>
            <div class="grid_4">
              <h2 class="blue">Hubungi Kami Segera</h2>
              <div class="box">
                <p>Kami selalu siap dengan segala pertanyaan dan konsultasi mengenai Perusahaan kami : </p>
                <br>
                <p>Yahoo Messenger :</p>
                <div class="ym">
                  <a href="ymsgr:sendIM?bahanafantasticpool"> <img src="http://opi.yahoo.com/online?u=bahanafantasticpool&m=g&t=2&l=us" alt="bahana"/></a>
                </div>
                
                
              </div>
            </div>
            <div class="clear"></div>

          </div>
        </div>
        
      </div>
      <?php include"footer.php"; ?>
      <script type="text/javascript">
        $(window).ready(function() {
          $('.nivo-slider').nivoSlider();
        });
      </script>
    </body>
</html>