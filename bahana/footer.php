<div class="wrapper footer-wrapper gradien-blue">
  <div id="footer" class="container_12">
    <div class="grid_12">
      <div class="footer-contact left">
        <div class="footer-menu">
          <ul>
            <li><a href="index.php">HOME</a></li>
            <li><a href="product.php">PRODUCT</a></li>
            <li class="split-last"><a href="contact.php">CONTACT</a></li>
          </ul>
        </div>
        <p>Silahkan hubungi kami untuk konsultasi dan pemesanan :</p>
        <table>
          <tr>
            <td>Call Now</td>
            <td>:</td>
            <td>024 702 27 445, 081 664 2975</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>PIN</td>
            <td>:</td>
            <td>276DF303</td>
          </tr>
        </table>
      </div>
      <div class="social right">
        <h4>Social Media</h4>
        <ul>
          <li><a href="http://www.facebook.com/bahana.fantasticpool"target=_blank><img src="images/facebook.jpg" alt="facebook"></a></li>
          <div class="clear"></div>
        </ul>
      </div>
    </div>
    <div class="clear"></div>
    <div class="grid_12 main-footer">
      <div class="copyright left">
        <p>Copyright &copy; 2013</p>
        <p>Hak cipta terhadap seluruh media yang terdapat di dalam website ini adalah milik  BAHANA FANTASTIC POOL</p>
      </div>
      <div class="paradise right">
        <a href="http://rectmedia.com" target=_blank><img src="images/paradise.png" alt="Cloud Paradise | Website Murah"></a>
      </div>
    </div>
    <div class="clear"></div> 
  </div>
</div>