<?php
      /**
       * Script untuk Send Email
       */
      $strError = "";
      //email pengirim
      $email_submit = "venario@rectmedia.com";

      //BILA PAKAI RECAPTCHA
     /* require_once('./js/recaptchalib.php');
      //$options = get_option( 'recaptcha_options' );
      $publickey = '6LdRf8kSAAAAAOWawp99hLoQLPnOBkktantW9fJ3';
      $privatekey = '6LdRf8kSAAAAAGsfoMgupEcsnZYfezDDZrqWjUW_';
      
     
      //the response from reCAPTCHA
      $resp = null;
      # the error code from reCAPTCHA, if any
      $error = null;
      //SAMPAI DI SINI RECAPTCHA*/
      if (isset($_POST['action']) == "send") {
      $name = $_POST["name"];
      $email = $_POST["email"];
      $telepon = $_POST["phone"];
      $subject = $_POST["subject"];
      $message = $_POST["pesan"];  
      $strMessage = '
              Hi,
              <br />
              Seseorang mengirim email melalui Form Contact dari website www.priority.com, berikut detailnya :
              <br />
              Data Diri :
              <table width="400px" style="margin-left:40px">
              <tr><td width="30%">Nama :</td><td>'.$name.'</td></tr>
              <tr><td width="30%">Email :</td><td>'.$email.'</td></tr>
              <tr><td width="30%">Telepon :</td><td>'.$telepon.'</td></tr>
              </table>
              <br />
              Pesan yang disampaikan :
              <table width="400px" style="margin-left:40px">
              <tr>
                <td width="30%">Judul :</td><td>'.$subject.'</td>
              </tr>
              <tr>
                <td width="30%">Pesan :</td><td>'.$message.'</td>
              </tr>
              </table>
          ';

        $strTo = $email_submit;
        $strSubject = '[PARADISE] Anda mendapatkan pesan dari '.$name.' melalui portal pesan www.priority.com';
        $strMessage = nl2br($strMessage);

        $strSid = md5(uniqid(time()));

        $strHeader = "";
        $strHeader .= "From: ".$name."<".$email.">\nReply-To: ".$email."\n";

        $strHeader .= "MIME-Version: 1.0\n";
        $strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\n\n";
        $strHeader .= "This is a multi-part message in MIME format.\n";

        $strHeader .= "--".$strSid."\n";
        $strHeader .= "Content-type: text/html; charset=utf-8\n";
        $strHeader .= "Content-Transfer-Encoding: 7bit\n\n";
        $strHeader .= $strMessage."\n\n";

              # was there a reCAPTCHA response?
              # JIKA PAKAI RECAPTCHA
          /*  if ($_POST["recaptcha_response_field"]) {
                      $resp = recaptcha_check_answer ($privatekey,
                                                      $_SERVER["REMOTE_ADDR"],
                                                      $_POST["recaptcha_challenge_field"],
                                                      $_POST["recaptcha_response_field"]);

                      if ($resp->is_valid) {*/
                          $flgSend = @mail($strTo,$strSubject,null,$strHeader);  // @ = No Show Error //

                          if($flgSend)
                          {
                                  $strError = '<h3 id="successmesage">Terima kasih, pesan berhasil terkirim</h3>';
                          }
                          else
                          {
                                  $strError = '<h3 id="errormessage">Maaf, pesan gagal terkirim</h3>';
                          }
              // JIKA PAKAI RECAPTCHA              
              /*                          
                      } else {
                              # set the error code so that we can display it
                              $error = $resp->error;
                              $strError = "<br><center><h2>".$error."</h2></center><br>";
                      }
              }*/

      }
      
      ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>PRIORITY - Air Minum</title>
      <meta name="description" content="CV PRIORITY INTI RAYA didirikan di Semarang Tgl 30 Jully 2011. Priority di dirikan karena adannya  kepedulian perusahaan kami akan kesehatan masyarakat di Indonesia. Kepedulian kami terhadap kesehatan masyarakat salah satunya adalah kebiasaan sehari hari kita untuk mengkonsumsi air minum yang berkualitas untuk menjaga kesehatan kita"/>
      <meta name="keywords" content="priority, air minum, PRIORITY INTI RAYA"/>
      <meta name="robots" content="index,follow" />
      <meta name="GOOGLEBOT" content="archive" />
      <meta name="author" content="Cloud Paradise"/>
      <link rel="image_src" href="images/priority.jpg" />
      <link rel="icon" href="images/favicon.png"/>
      <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css' />
      <link rel="stylesheet" type="text/css" href="style/960.css" />
      <link rel="stylesheet" type="text/css" href="style/style.css" />
      <link rel="stylesheet" type="text/css" href="js/validationEngine.jquery.css" />
      <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHAd5EtGvzkpyMJ3C_qNEb81ujYCB7rEA&sensor=false"></script>
      <script type="text/javascript" src="js/map.js"></script> 
      <script type="text/javascript" src="js/jquery.validationEngine.js"></script> 
      <script type="text/javascript" src="js/jquery.validationEngine-en.js"></script> 
      <script type="text/javascript">
        $(document).ready(function() {
          initializeMap();
          $("form#contact_form").validationEngine();
        });
        
      </script>   
    </head>
    
    <body>
      <img id="main-bg" src="images/bg-body.jpg">
      <div class="wrapper">
        <div class="container_12">
          <?php include"header.php";?>
          <div class="grid_12"><div class="separator"></div></div>
          <div class="clear"></div>
          <!-- content begin -->
          
          <div id="content">
            <div class="grid_12">
              <div class="page-wrapper contact">
                <div class="grid_12 alpha omega"><?php echo $strError; ?></div>
                <div class="grid_4 omega margin-top contact-left">
                  <div class="wrapper-left">
                    <h3>Contact Us</h3>
                    <div class="img-box">
                      <img src="images/contact-thumb.jpg">
                    </div>
                    <h5>Head Office</h5>
                    <p>Jalan Kawasan Industri<br> Candi Gatot Subroto 50146,<br> Semarang.</p>
                    <table>
                      <tr>
                        <td>Phone</td>
                        <td>:</td>
                        <td>024.7027.7168,</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td>0888.3988.619</td>
                      </tr>
                      <tr>
                        <td>Fax</td>
                        <td>:</td>
                        <td>024.8441.088</td>
                      </tr>
                    </table>
                    <h5>Branch Office</h5>
                    <p>Jalan Royal Residence,<br> Harewood B-9/120, Surabaya.</p>
                    
                    </table>
                  </div>
                </div>
                <div class="grid_8 omega">
                  <div class="contact-right">
                    <h4>Hubungi Kami segera</h4>
                    <div id="sitemap"></div>
                    <p><strong>CV PRIORITY INTI RAYA</strong> siap membantu Anda, silahkan sampaikan pesan, kritik saran maupun masalah yang ingin Anda tanyakan kepada kami :</p>                  
                    <form id="contact_form" name="contact_form" action="http://localhost/repository/priority/contact.php" method="post">
                      <input type="hidden" name="action" value="send" />
                      <table>
                        <tr>
                          <td><label>Nama</label></td>
                          <td>:</td>
                          <td><input type="text" name="name" id="name" class="textfield validate[required]" value=""></td>
                        </tr>
                        <tr>
                          <td><label>Email</label></td>
                          <td>:</td>
                          <td><input type="text" name="email" id="email" class="textfield validate[required,custom[email]]"></td>
                        </tr>
                        <tr>
                          <td><label>Telepon</label></td>
                          <td>:</td>
                          <td><input type="text" name="phone" id="phone" class="textfield validate[required,maxSize[20]]"></td>
                        </tr>
                        <tr>
                          <td><label>Subject</label></td>
                          <td>:</td>
                          <td><input type="text" name="subject" id="subject" class="textfield validate[required]"></td>
                        </tr>
                        <tr>  
                          <td style="vertical-align: top;padding-top:10px;"><label>Pesan</label></td>
                          <td style="vertical-align: top;padding-top:10px;">:</td>
                          <td><textarea name="pesan" class="textfield validate[required]" id="pesan" type="textarea"></textarea></td>
                        </tr> 
                        <tr>
                          <td></td>
                          <td></td>
                          <td><input type="submit" class="button right" value="Submit"></td>
                        </tr>
                      </table>
                    </form>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <?php include"footer.php"; ?>
        </div>
      </div>

    </body>
</html>
