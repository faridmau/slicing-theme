var id = "sitemap";
var myLatLng = new google.maps.LatLng(-6.992807,110.366796);
var myZoom = 15;
var image = 'images/favicon.ico'; /* http://mapicons.nicolasmollet.com/ */
var markerTitle = "Kota Semarang";
var markers = "PT. Saudara Makmur<br />Pangkalan Truk Genuk D-1<br />Jl. Raya Kaligawe Km. 5<br />Semarang";


function initializeMap(){
	var mapOptions = {
      center: myLatLng,
      zoom: myZoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById(id),
        mapOptions);

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: markerTitle,
      icon: image,
	});
}

  



