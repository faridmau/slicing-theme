<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>PRIORITY - Air Minum</title>
      <meta name="description" content="CV PRIORITY INTI RAYA didirikan di Semarang Tgl 30 Jully 2011. Priority di dirikan karena adannya  kepedulian perusahaan kami akan kesehatan masyarakat di Indonesia. Kepedulian kami terhadap kesehatan masyarakat salah satunya adalah kebiasaan sehari hari kita untuk mengkonsumsi air minum yang berkualitas untuk menjaga kesehatan kita"/>
      <meta name="keywords" content="priority, air minum, PRIORITY INTI RAYA"/>
      <meta name="robots" content="index,follow" />
      <meta name="GOOGLEBOT" content="archive" />
      <meta name="author" content="Cloud Paradise"/>
      <link rel="image_src" href="images/priority.jpg" />
      <link rel="icon" href="images/favicon.png"/>
      <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css' />
      <link rel="stylesheet" type="text/css" href="style/960.css" />
  		<link rel="stylesheet" type="text/css" href="style/style.css" />
      <link rel="stylesheet" type="text/css" href="style/nivo-slider/nivo-slider.css" />
      <link rel="stylesheet" type="text/css" href="style/nivo-slider/default.css" />
      <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
      <script type="text/javascript" src="js/nivo-slider/jquery.nivo.slider.js"></script>
      <script type="text/javascript" src="js/nivo-slider/jquery.nivo.slider.pack.js"></script>
    </head>
    
    <body>
      <img id="main-bg" src="images/bg-body.jpg" alt="background" />
      <div class="wrapper">
        
        <div class="container_12">
          <?php include"header.php";?>
          <div class="clear"></div>
          <!-- slider begin -->
          <div id="slider" class="slider-wrapper">
            <div class="grid_12 theme-default">
              <div class="nivo-slider">
                <img src="images/slide1.jpg" alt="slide-one"/>
                <img src="images/slide2.jpg" alt="slide-two"/>
                <img src="images/slide3.jpg" alt="slide-three"/>
                <img src="images/slide4.jpg" alt="slide-four"/>
                <img src="images/slide5.jpg" alt="slide-five"/>
              </div>
            </div>
            <div class="clear"></div>  
          </div>
          <div id="content">
            <div class="grid_12">
              <div class="content-wrapper">
                <h2>Good solutions for your life</h2>
                <div class="clear"></div>
                <div class="grid_8">
                  <div class="wrapper-left">
                    <div class="img-box left img-rightmargin">
                      <img class="margin-left" src="images/pict1.jpg" alt="priority-image"/>
                    </div>
                    <p>Kelebihan dari air minum yang kita produksi adalah air minum kami bebas dari kandungan kandungan yang tidak berguna di dalam tubuh sehingga air minum kita lebih mudah di serap oleh tubuh. Keunggulan lainnya adalah air minum kita di lengkapi oleh kadar Oksigen yang tinggi dan juga menggunakan sistem UV <span class="italic">Ultra Violet</span> kesterilan dan kemurnian air kita.</p>
                    <p>Dengan mengkonsumsi Air Minum Priority maka tubuh kita akan menjadi lebih sehat, berbagai penyakit sembuh dan juga lebih awet muda. Mari kita tingkatkan kesehatan dan juga kualitas hidup kita untuk menjadi lebih baik bersama Air Minum Priority.</p>
                    <p>PRIORITY menggunakan air mata air asli gunung Ungaran , itupun  melalui test laboratorium dan di pantau oleh SNI karena kita hanya memilih mata air yang terbaik untuk dapat             dikonsumsi.</p>
                    <p>PRIORITY mengunakan pencucian secara manual dengan diteliti ketat oleh QC (<span class="italic">Quality Control</span>)  setelah itu lanjut ke pencucian secara <span class="italic">automatic</span> dengan mesin lalu di semprot dengan air panas yang telah di sterilkan supaya bersih dari bakteri, lalu dibersihkan dengan mesin penyikat otomatis setelah bersih dari kotoran di semprot lagi dengan air RO supaya bersih dari air sisa pencucian. Dan juga tenaga kita memakai masker penutup mulut sarung tangan dan sepatu bots untuk kebersihan tenaga pencuci kita.</p>
                  </div>
                </div>
                <div class="grid_4 alpha omega">
                  <div class="right-content wrapper-right">
                    <h3>Manfaat Air Minum Priority</h3>
                    <ul>
                      <li>Mengandung Kadar Oksigen Tinggi</li>
                      <li>Meningkatkan Daya Ingat</li>
                      <li>Meningkatkan Kecerdasan Otak</li>
                      <li>Meningkatkan Metabolisme</li>
                      <li>Menstabilkan Tekanan Darah</li>
                      <li>Memperkuat Jantung</li>
                      <li>Mempercantik Kulit</li>
                      <li>Menstabilkan Gula Darah</li>
                    </ul>
                    <div class="readmore">
                      <a href="product.php">BACA SELENGKAPNYA</a>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <?php include"footer.php"; ?>
        </div>
      </div>

      <script type="text/javascript">
        $(window).ready(function() {
          $('.nivo-slider').nivoSlider();
        });
      </script>
    </body>
</html>
