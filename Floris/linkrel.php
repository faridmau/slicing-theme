<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Vivi Florist | Artificial & Fresh Flowers, Stick Werk</title>
<meta name="description" content="Floris"/>
<meta name="keywords" content="Floris"/>
<meta name="robots" content="index,follow" />
<meta name="GOOGLEBOT" content="archive" />
<meta name="author" content="Cloud Paradise"/>
<link rel="image_src" href="images/paradise.jpg" />
<link rel="icon" href="images/favicon.png"/>
<link rel="stylesheet" type="text/css" href="style/960.css" />
<link rel="stylesheet" type="text/css" href="style/style.css" />
<link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>