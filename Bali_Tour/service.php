<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <?php include"linkrel.php"; ?>
      <script type="text/javascript" src="js/sticky/stickyfloat.js"></script>
     
    </head>
    
    <body>
    	<?php include"header.php"; ?>
      <div class="wrapper radius10" id="content">
        <div class="service container_12">
          <img src="images/thumb-top.png" class="thumb-top">
          <div class="grid_8 prefix_4">
            <h3 class="orange">Layanan Bali Tour</h3>
          </div>
          <div class="clear"></div>
          <div class="service-wrapper">
            <div class="grid_4 left left-service">
              <div>
                <h3 class="blue">Categories</h3>
                <ul>
                  <li><a href="#">Layanan Bali Tour</a></li>
                  <li><a href="#">Armada Bali Tour</a></li>
                  <li><a href="#">Tempat Tujuan</a></li>
                  <li><a href="#">Relasi Bali Tour</a></li>
                </ul>
              </div>
            
            </div>
            <div class="grid_8 right">
              <div class="right-service">
                <div class="layanan sub-title">
                  <p>Augue aliquet pellentesque vut lacus eros integer in, natoque, natoque et mid phasellus mid, nunc placerat? Habitasse proin nisi nunc, odio ultrices! Auctor magna, integer augue vut enim lorem penatibus mauris tincidunt cursus augue vut mauris, nisi ac. Urna nascetur sit habitasse tincidunt sit in turpis a enim scelerisque in. Augue natoque pid nascetur a porta, ridiculus turpis a elit.</p>
                  <p>Augue aliquet pellentesque vut lacus eros integer in, natoque, natoque et mid phasellus mid, nunc placerat? Habitasse proin nisi nunc, odio ultrices! Auctor magna, integer augue vut enim lorem penatibus mauris tincidunt cursus augue vut mauris, nisi ac. Urna nascetur sit habitasse tincidunt sit in turpis a enim scelerisque in. Augue natoque pid nascetur a porta, ridiculus turpis a elit.</p>
                  
                  <ul class="blue">
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                  </ul>     
                </div>
                <div class="armada sub-title">
                  <h3 class="orange">Armada Bali Tour</h3>
                  <p>Augue aliquet pellentesque vut lacus eros integer in, natoque, natoque et mid phasellus mid, nunc placerat? Habitasse proin nisi nunc, odio ultrices! Auctor magna, integer augue vut enim lorem penatibus mauris tincidunt cursus augue vut mauris, nisi ac. Urna nascetur sit habitasse tincidunt sit in turpis a enim scelerisque in. Augue natoque pid nascetur a porta, ridiculus turpis a elit.</p>
                  <div class="imgs left right-margin"><img src="images/pict7.jpg"></div>
                  
                  <ul class="blue left">
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                  </ul>
                  <div class="clear"></div>     
                </div>
                <div class="tujuan sub-title">
                  <h3 class="orange">Tempat Tujuan</h3>
                  <p>Augue aliquet pellentesque vut lacus eros integer in, natoque, natoque et mid phasellus mid, nunc placerat? Habitasse proin nisi nunc, odio ultrices! Auctor magna, integer augue vut enim lorem penatibus mauris tincidunt cursus augue vut mauris, nisi ac. Urna nascetur sit habitasse tincidunt sit in turpis a enim scelerisque in. Augue natoque pid nascetur a porta, ridiculus turpis a elit.</p>
                  
                  <ul class="blue left">
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                  </ul>     
                  <div class="imgs right left-margin"><img src="images/pict8.jpg"></div>
                  <div class="clear"></div>
                </div>
                <div class="tujuan sub-title">
                  <h3 class="orange">Relasi Bali Tour</h3>
                  <p>Augue aliquet pellentesque vut lacus eros integer in, natoque, natoque et mid phasellus mid, nunc placerat? Habitasse proin nisi nunc, odio ultrices! Auctor magna, integer augue vut enim lorem penatibus mauris tincidunt cursus augue vut mauris, nisi ac. Urna nascetur sit habitasse tincidunt sit in turpis a enim scelerisque in. Augue natoque pid nascetur a porta, ridiculus turpis a elit.</p>
                  <div class="imgs left right-margin"><img src="images/pict9.jpg"></div>
                  <p>Augue aliquet pellentesque vut lacus eros integer in, natoque, natoque et mid phasellus mid, nunc placerat? Habitasse proin nisi nunc, odio ultrices! Auctor magna, integer augue vut enim lorem penatibus mauris tincidunt cursus augue vut mauris, .</p>
                  <div class="clear"></div>
                  <ul class="blue">
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                    <li>Fusce aliquet pede non pede.</li>
                  </ul>  
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
        </div>

      </div>
    	<?php include"footer.php"; ?>
			<script type="text/javascript">
				$(document).ready(function(){
  				jQuery('.left-service').stickyfloat( {duration: 400} );
				});
			</script>    	
    </body>
</html>
