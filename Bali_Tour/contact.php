<?php
  /**
   * Script untuk Send Email
   */
  $strError = "";
  //email pengirim
  $email_submit = "venario@rectmedia.com";

  //BILA PAKAI RECAPTCHA
 /* require_once('./js/recaptchalib.php');
  //$options = get_option( 'recaptcha_options' );
  $publickey = '6LdRf8kSAAAAAOWawp99hLoQLPnOBkktantW9fJ3';
  $privatekey = '6LdRf8kSAAAAAGsfoMgupEcsnZYfezDDZrqWjUW_';
  
 
  //the response from reCAPTCHA
  $resp = null;
  # the error code from reCAPTCHA, if any
  $error = null;
  //SAMPAI DI SINI RECAPTCHA*/
  if (isset($_POST['action']) == "send") {
  $name = $_POST["name"];
  $email = $_POST["email"];
  $telepon = $_POST["phone"];
  $subject = $_POST["subject"];
  $message = $_POST["pesan"];  
  $strMessage = '
          Hi,
          <br />
          Seseorang mengirim email melalui Form Contact dari website www.xx.com, berikut detailnya :
          <br />
          Data Diri :
          <table width="400px" style="margin-left:40px">
          <tr><td width="30%">Nama :</td><td>'.$name.'</td></tr>
          <tr><td width="30%">Email :</td><td>'.$email.'</td></tr>
          <tr><td width="30%">Telepon :</td><td>'.$telepon.'</td></tr>
          </table>
          <br />
          Pesan yang disampaikan :
          <table width="400px" style="margin-left:40px">
          <tr>
            <td width="30%">Judul :</td><td>'.$subject.'</td>
          </tr>
          <tr>
            <td width="30%">Pesan :</td><td>'.$message.'</td>
          </tr>
          </table>
      ';

    $strTo = $email_submit;
    $strSubject = '[PARADISE] Anda mendapatkan pesan dari '.$name.' melalui portal pesan www.xx.com';
    $strMessage = nl2br($strMessage);

    $strSid = md5(uniqid(time()));

    $strHeader = "";
    $strHeader .= "From: ".$name."<".$email.">\nReply-To: ".$email."\n";

    $strHeader .= "MIME-Version: 1.0\n";
    $strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\n\n";
    $strHeader .= "This is a multi-part message in MIME format.\n";

    $strHeader .= "--".$strSid."\n";
    $strHeader .= "Content-type: text/html; charset=utf-8\n";
    $strHeader .= "Content-Transfer-Encoding: 7bit\n\n";
    $strHeader .= $strMessage."\n\n";

          # was there a reCAPTCHA response?
          # JIKA PAKAI RECAPTCHA
      /*  if ($_POST["recaptcha_response_field"]) {
                  $resp = recaptcha_check_answer ($privatekey,
                                                  $_SERVER["REMOTE_ADDR"],
                                                  $_POST["recaptcha_challenge_field"],
                                                  $_POST["recaptcha_response_field"]);

                  if ($resp->is_valid) {*/
                      $flgSend = @mail($strTo,$strSubject,null,$strHeader);  // @ = No Show Error //

                      if($flgSend)
                      {
                              $strError = '<h3 id="successmessage">Terima kasih, pesan berhasil terkirim</h3>';
                      }
                      else
                      {
                              $strError = '<h3 id="errormessage">Maaf, pesan gagal terkirim</h3>';
                      }
          // JIKA PAKAI RECAPTCHA              
          /*                          
                  } else {
                          # set the error code so that we can display it
                          $error = $resp->error;
                          $strError = "<br><center><h2>".$error."</h2></center><br>";
                  }
          }*/

  }
  
  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <?php include"linkrel.php"; ?>
      <link rel="stylesheet" type="text/css" href="style/validationEngine.jquery.css" />
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHAd5EtGvzkpyMJ3C_qNEb81ujYCB7rEA&sensor=false"></script>
      <script type="text/javascript" src="js/map.js"></script>
      <script type="text/javascript" src="js/jquery.validationEngine-en.js"></script>
      <script type="text/javascript" src="js/jquery.validationEngine.js"></script>

    </head>
    
    <body>
    	<?php include"header.php"; ?>
    	
    	<div class="wrapper">
        <div class="grid_12">
          
        </div>
        
      </div>
      
      <div class="wrapper radius10" id="content">
        
        <div class="contact container_12">
          <img src="images/thumb-top.png" class="thumb-top">
          <div class="grid_12">
            <h3 class="orange">Contact Us</h3>
          </div>
          <div class="clear"></div>
          <div class="grid_8">
            
            <div id="map"></div>
            <p>PT TINIGA BALI TOUR siap membantu Anda, silahkan sampaikan pesan, kritik saran maupun masalah yang ingin Anda tanyakan kepada kami :</p>
            <div class="error">
              <!-- error -->
            </div>
            <div class="form">
              <form id="contact-form" name="contact_form" action="contact.php" method="post">
                <input type="hidden" name="action" value="send" />
                <div class="field">
                  <label>Nama</label>
                  <span>:</span>
                  <input type="text" name="name" class="validate[required]" value="">
                  <div class="clear"></div>
                </div>
                <div class="field">
                  <label>Email</label>
                  <span>:</span>
                  <input type="text" name="email" class="validate[required,custom[email]]">
                  <div class="clear"></div>
                </div>
                <div class="field">
                  <label>Telepon</label>
                  <span>:</span>
                  <input type="text" name="phone" class="validate[required,custom[integer],maxSize[20]]">
                  <div class="clear"></div>
                </div>
                <div class="field">
                  <label>Subjek</label>
                  <span>:</span>
                  <input type="text" name="subject" class="validate[required]">
                  <div class="clear"></div>
                </div>
               
                <div class="field">
                  <label>Pesan</label>
                  <span class="v-align">:</span>
                  <textarea name="pesan" class="validate[required]"></textarea>

                </div>
                <div class="field"><input type="button" value="Submit" class="button orange-grad radius10 right submit"/></div>
              </form>
            </div>

          </div>
          <div class="grid_4">
            <div class="contact-pict">
              <img src="images/pict6.jpg">
            </div>
            <div class="right-align">
              <h5 class="blue">Kantor Pusat</h5>
              <p>Jl. Sompok Baru 84 A Semarang, 50249</p>
              <div class="field">
                <label class="left left-align">Phone</label>
                <span class="left">:</span>
                <p class="left right-align">(024) 831.3701</p>
                <div class="clear"></div>
              </div>
              <div class="field">
                <label class="left left-align">Fax</label>
                <span class="left">:</span>
                <p class="left right-align">(024) 831.0488</p>
                <div class="clear"></div>
              </div>
              <div class="field">
                <label class="left left-align">Email</label>
                <span class="left">:</span>
                <p class="left right-align">tinigabalitur@gmail.com</p>
                <div class="clear"></div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
        </div>

      </div>
      
    	<?php include"footer.php"; ?>
			<script type="text/javascript">
				$(document).ready(function(){
  				jQuery("#contact-form").validationEngine();
          initializeMap();
        });
			</script>    	
    </body>
</html>
