<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <?php include"linkrel.php"; ?>
      <!-- call bxslider plugin -->
      <script type="text/javascript" src="js/bxslider/jquery.bxslider.js"></script>
      <script type="text/javascript" src="js/bxslider/jquery.bxslider.min.js"></script>
      <script type="text/javascript" src="js/bxslider/jquery.easing.1.3.js"></script>
      <script type="text/javascript" src="js/bxslider/jquery.fitvids.js"></script>
      <link rel="stylesheet" type="text/css" href="style/bxslider/jquery.bxslider.css">

      <!-- call fancybox -->
      <link rel="stylesheet" href="style/fancybox/jquery.fancybox.css" type="text/css" media="screen">
      
      <script type="text/javascript" src="js/fancybox/jquery.fancybox.pack.js"></script>
      <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>

      <!-- optional -->
      <link rel="stylesheet" href="style/fancybox/jquery.fancybox-buttons.css" type="text/css" media="screen">
      <script type="text/javascript" src="js/fancybox/jquery.fancybox-buttons.js"></script>
      <script type="text/javascript" src="js/fancybox/jquery.fancybox-media.js"></script>

      <link rel="stylesheet" href="style/fancybox/jquery.fancybox-thumbs.css" type="text/css" media="screen">
      <script type="text/javascript" src="js/fancybox/jquery.fancybox-thumbs.js"></script>
    </head>
    
    <body>
    	<?php include"header.php"; ?>
    	<div class="wrapper" id="slider">
    		<div class="container_12">
    			<div class="grid_12">
    				<ul class="text-bxslider">
						  <li>
						  	<h6>You’ll never forget the</h6>
						  	<h6>experience you’ll find here!</h6>
						  </li>
						  <li>
						  	<h6>2.You’ll never forget the</h6>
						  	<h6>experience you’ll find here!</h6>
						  </li>
						  <li>
						  	<h6>3.You’ll never forget the</h6>
						  	<h6>experience you’ll find here!</h6>
						  </li>
						</ul>
    			</div>
    			<div class="clear"></div>
    		</div>
    	</div>
    	<div class="wrapper white-trans radius10" id="carousel">
				<div class="slider2">
			    <div class="slide"><a class="fancybox-thumb" rel="fancybox-thumb" href="images/pict1.jpg" title="Porth Nanven (MarcElliott)"><img src="images/pict1.jpg"></a></div>
			    <div class="slide"><a class="fancybox-thumb" rel="fancybox-thumb" href="images/pict2.jpg" title="Porth Nanven (MarcElliott)"><img src="images/pict2.jpg"></a></div>
			    <div class="slide"><a class="fancybox-thumb" rel="fancybox-thumb" href="images/pict3.jpg" title="Porth Nanven (MarcElliott)"><img src="images/pict3.jpg"></a></div>
			    <div class="slide"><a class="fancybox-thumb" rel="fancybox-thumb" href="images/pict4.jpg" title="Porth Nanven (MarcElliott)"><img src="images/pict4.jpg"></a></div>
			    <div class="slide"><a class="fancybox-thumb" rel="fancybox-thumb" href="images/pict5.jpg" title="Porth Nanven (MarcElliott)"><img src="images/pict5.jpg"></a></div>
				  
				</div>
    		
    	</div>
    	<div class="wrapper radius10" id="content">
    		<div class="feature">
    			<div class="wrapper-box">
    				<div class="boxs">
    					<div class="feature-title fea-first"><h4>Enjoy your vacation</h4><h4>with us!</h4></div>
    					<div class="text">
    						<div class="feature-text"><p>Dolor cum mattis nec odio non pellentesque aenean scelerisque nunc turpis vel vel amet, nisi vut turpis in cum magna penatibus nunc sed eros, nunc! Diam porta, ridiculus augue duis.</p></div>
	    					<div class="feature-button center"><a href="#" class="button orange-grad radius10">more</a></div>
    					</div>
    				</div>
    				<div class="boxs-midle">
    					<div class="feature-title"><h4>Enjoy your vacation</h4><h4>with us!</h4></div>
    					<div class="text">
    						<div class="feature-text"><p>Dolor cum mattis nec odio non pellentesque aenean scelerisque nunc turpis vel vel amet, nisi vut turpis in cum magna penatibus nunc sed eros, nunc! Diam porta, ridiculus augue duis.</p></div>
	    					<div class="feature-button center"><a href="#" class="button orange-grad radius10">more</a></div>
    					</div>
    				</div>
    				<div class="boxs border-last">
    					<div class="feature-title fea-last"><h4>Enjoy your vacation</h4><h4>with us!</h4></div>
    					<div class="text">
    						<div class="feature-text"><p>Dolor cum mattis nec odio non pellentesque aenean scelerisque nunc turpis vel vel amet, nisi vut turpis in cum magna penatibus nunc sed eros, nunc! Diam porta, ridiculus augue duis.</p></div>
	    					<div class="feature-button center"><a href="#" class="button orange-grad radius10">more</a></div>
    					</div>
    				</div>
    				<div class="clear"></div>
    			</div>
    		</div>
    		<div class="container_12">
    			<div class="grid_8">
    				<h3 class="orange">Welcome to Our Site</h3>
    				<p>PT.Tiniga Bali Tour is a company engaged in the field of tour and travel services located in Semarang, Central Java. Pt Tiniga Bali Tour was founded in 1989 and has served many consumers with a variety of destinations both domestically and overseas. </p>
    				<p>Since establishment the company has been serving all the agencies, school students and college students. Good for tourism events, KKL and visit both located in and outside Semarang Semarang</p>
    				<div class="read-more right"><a href="#" class="blue italic">read more...</a></div>
    			</div>
    			<div class="grid_4">
    				<h3 class="blue">Hubungi Kami Segera</h3>
    				<div class="box radius10 blue-grad">
    					<p>Kami selalu siap dengan segala pertanyaan dan konsultasi mengenai Perusahaan kami :</p>
    				</div>
    			</div>
    			<div class="clear"></div>
    		</div>
    	</div>
    	<!-- <img src="images/footer-bg.jpg" class="foo-bg"> -->
    	<?php include"footer.php"; ?>
			<script type="text/javascript">
				$(document).ready(function(){
  				$('.text-bxslider').bxSlider({
            auto: true,
            mode: 'fade',
            preloadImages: 'visible'
          });

  				// image crausel
  				$('.slider2').bxSlider({
				    // slideWidth: 50,
				    minSlides: 5,
				    maxSlides: 5,
				    moveSlides: 1

				  });

				  $(".fancybox-thumb").fancybox({
						
						helpers		: {
							title	: { type : 'inside' },
							buttons	: {}
						}
					});
				});
			</script>    	
    </body>
</html>
